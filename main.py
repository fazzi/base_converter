#!/usr/bin/env python

# base_converter

# This program aims to guess which base the users input is
# (out of binary, denary, octal, and hexadecimal)
# It will then print all possibile conversions of the input
# to all necessary bases.
# Data from user may be ambiguous, e.g. the input `0101`
# could be denary, binary, octal or hexadecimal. We must
# account for all cases.

# for sys.exit()
import sys
# Each output for each possible base is stored in `outputs.py`
from outputs import *


binary_possibilities = ["0", "1"]
denary_possibilities = ["2", "3", "4", "5", "6", "7", "8", "9"]
octal_possibilities = ["0", "1", "2", "3", "4", "5", "6", "7"]
hexa_possibilities = ["A", "B", "C", "D", "E", "F"]

denary_possibilities = binary_possibilities + denary_possibilities
hexa_possibilities = denary_possibilities + hexa_possibilities


def main():
    # we use .strip() to remove whitespace from user input
    user_input = str(input("Enter your number: ")).strip()

    # Test if hexadecimal or not. hexa contains letters,
    # so if .isdigit() is false, it cannot be denary,
    # binary or octal.
    if user_input.isdigit():
        binary_possible = True
        octal_possible = True
        for char in user_input:
            # If digits other than 0 or 1 exist
            if char not in binary_possibilities:
                binary_possible = False
            # If digits other than 0-7 exist
            if char not in octal_possibilities:
                octal_possible = False
        if binary_possible and octal_possible:
            print("Could be denary OR binary OR octal OR hexadecimal\n")
            denary_in(user_input)
            binary_in(user_input)
            octal_in(user_input)
            hexa_in(user_input)
        elif octal_possible:
            print("Could be denary OR octal OR hexadecimal\n")
            denary_in(user_input)
            octal_in(user_input)
            hexa_in(user_input)
        else:
            print("Could be denary OR hexadecimal\n")
            denary_in(user_input)
            hexa_in(user_input)
    else:
        impossible_input = False
        for char in user_input:
            # If digits other than 0-9, A-F exist, input is impossible.
            # We check with .upper to remove issues with capitalization
            if char.upper() not in hexa_possibilities:
                impossible_input = True
        # If the user enters nothing or input other than
        # 0-9, A-F, it cannot be valid input
        if impossible_input or user_input == "":
            print("Not a possible input\n")
        else:
            print("Can only be hexadecimal\n")
            hexa_in(user_input)
    # Separate Outputs a little bit
    print("")


if __name__ == "__main__":
    while True:
        try:
            main()
        except KeyboardInterrupt:
            print("\nGoodbye.")
            sys.exit()
