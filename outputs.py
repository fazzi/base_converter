#!/usr/bin/env python

# This file contains the print statements for each input base.

# Each conversion function is stored in a separate `conversions.py`
from conversions import *


def denary_in(user_input):
    print(f"\nAssuming {user_input} is Denary...")
    print(f"Denary Input to Binary: {denary_to_bin(user_input)}")
    print(f"Denary Input to Hexadecimal: {denary_to_hexa(user_input)}")
    print(f"Denary Input to Octal: {denary_to_octal(user_input)}")


def binary_in(user_input):
    print(f"\nAssuming {user_input} is Binary...")
    print(f"Binary Input to Denary: {bin_to_denary(user_input)}")
    print(f"Binary Input to Hexadecimal: {bin_to_hexa(user_input)}")
    print(f"Binary Input to Octal: {bin_to_octal(user_input)}")


def octal_in(user_input):
    print(f"\nAssuming {user_input} is Octal...")
    print(f"Octal Input to Denary: {octal_to_denary(user_input)}")
    print(f"Octal Input to Binary: {octal_to_bin(user_input)}")
    print(f"Octal Input to Hexadecimal: {octal_to_hexa(user_input)}")


def hexa_in(user_input):
    print(f"\nAssuming {user_input} is Hexadecimal...")
    print(f"Hexadecimal Input to Denary: {hexa_to_denary(user_input)}")
    print(f"Hexadecimal Input to Binary: {hexa_to_bin(user_input)}")
    print(f"Hexadecimal Input to Octal: {hex_to_octal(user_input)}")
