#!/usr/bin/env python

# POSSIBILITIES
# denary to binary
# denary to hexa
# denary to octal
# binary to denary
# binary to hexa
# binary to octal
# octal to binary
# octal to hexa
# octal to denary
# hexa to denary
# hexa to binary
# hexa to octal

def denary_to_bin(local_user_input):
    binary_input = "{0:b}".format(int(local_user_input))
    # Add leading zeros to ensure a multiple of 4 bits
    while len(binary_input) % 4 != 0:
        binary_input = "0" + binary_input
    # Insert spaces to separate each nibble (4 bits)
    standardized_output = " ".join(
        [binary_input[i: i + 4] for i in range(0, len(binary_input), 4)]
    )
    return standardized_output


def denary_to_hexa(local_user_input):
    # [2:] here removes the 0x at the beginning
    # .upper so hexadecimal always comes out as A B C D E F
    local_user_input = hex(int(local_user_input))[2:].upper()
    # Add leading zeros to ensure a multiple of 4 bits
    while len(local_user_input) % 2 != 0:
        local_user_input = "0" + local_user_input
    # Insert spaces to separate each nibble (4 bits)
    standardized_output = " ".join(
        [local_user_input[i: i + 2]
            for i in range(0, len(local_user_input), 2)]
    )
    return standardized_output


def denary_to_octal(local_user_input):
    # [2:] here removes the 0o at the beginning
    return oct(int(local_user_input))[2:]


def bin_to_denary(local_user_input):
    return int(local_user_input, 2)


def bin_to_hexa(local_user_input):
    local_user_input = bin_to_denary(local_user_input)
    local_user_input = denary_to_hexa(local_user_input)
    return local_user_input


def bin_to_octal(local_user_input):
    local_user_input = int(local_user_input, 2)
    # [2:] here removes the 0o at the beginning
    return oct(local_user_input)[2:]


def octal_to_denary(local_user_input):
    return int(local_user_input, 8)


def octal_to_bin(local_user_input):
    local_user_input = int(local_user_input, 8)
    local_user_input = denary_to_bin(local_user_input)
    return local_user_input


def octal_to_hexa(local_user_input):
    local_user_input = int(local_user_input, 8)
    local_user_input = denary_to_hexa(local_user_input)
    return local_user_input


def hexa_to_denary(local_user_input):
    return int(local_user_input, 16)


def hexa_to_bin(local_user_input):
    local_user_input = hexa_to_denary(local_user_input)
    local_user_input = denary_to_bin(local_user_input)
    return local_user_input


def hex_to_octal(local_user_input):
    local_user_input = int(local_user_input, 16)
    # [2:] here removes the 0o at the beginning
    return oct(local_user_input)[2:]
