# base_converter

A simple base converter made in python.

It attempts to guess what base the users input is (out of denary, binary, octal, and hexadecimal) and print all possible outputs for each.
